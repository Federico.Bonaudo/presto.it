<x-layout>


    <div class="container-fluid bg-grey py-5 min-vh-100">
    <header>
        <div class="container py-5 my-4">
            <div class="row-justify-content-center">
                <div class="col-12 text-center ff-p">
                    <div>
                        <span class="tx-s tx-shadow display-5">  {{__('ui.profile')}} </span>
                        <span class="tx-p tx-shadow display-5 mx-2">  {{__('ui.profile1')}} , </span>
                        <span class="tx-s tx-shadow display-5">{{Auth::user()->name}}!</span>

                    </div>
                    <p class="tx-s lead mt-5 ff-s">{{__('ui.profile2')}}</p>
                </div>
            </div>
        </div>
    </header>

    <div class="container my-5">
        <div class="row justify-content-center">
            @forelse ($user_articles as $article)
                @if($article->is_accepted != null && Auth::user()->id == $article->user_id)
                    <div class="col-12 col-md-4 mt-5 mb-3 d-flex justify-content-center">
                        <div class="card card-container text-center positione-relative">
                            <a href="{{route('category.show', $article->category)}}" class="text-decoration-none text-reset">
                                <p class="text-white small category-decoration"> {{ $article->category->name }}</p>
                            </a> 
                            <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(300, 300) : '../../media/no-image300x300.jpg'}}" alt="" class="img-top card-image">
                            <div class="card-body d-flex flex-column justify-content-between">
                            <h3 class="article-title text-white">{{ $article->title }}</h3>
                            <h2 class="price">€{{ $article->price }}</h2>
                            <a href="{{ route('article.show', compact('article')) }}" class="text-decoration-none ">
                                <button class=" bottone-card p-2 px-3">{{__('ui.card1')}}</button>
                            </a>
                            </div>
                        </div>
                    </div>
                @endif
            @empty
                <p class="text-danger">{{__('ui.card4')}}</p>
            @endforelse
        </div>
    </div>

    
</div>





</x-layout>