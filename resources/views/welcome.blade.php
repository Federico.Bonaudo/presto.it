<x-layout>

    <header class="container-fluid homepage">
        <x-session-messages />
        <div class="container h-100 mt-5 pt-5 position-relative">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-lg-6 text-center text-md-start position-relative">
                    <div class="inner-about-the-fold">
                        <h1 class="banner-prima-parte" data-aos="fade-right" data-aos-duration="800">{{__('ui.welcome')}}</h1>
                        <h2 class="banner-seconda-parte" data-aos="fade-right" data-aos-delay="200" data-aos-duration="800">{{__('ui.welcome2')}}</h2>
                        <div class="button-wrapper">
                            <h2 class="sub-banner my-5 ff-s" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">{{__('ui.welcome3')}}</h2>
                            <a href="{{route('article.index')}}"><button class="bottone-banner" data-aos="fade-right" data-aos-delay="600" data-aos-duration="800">{{__('ui.welcome4')}}</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 position-relative">
                    <img src="../../media/flippedRocket.png" data-aos="fade-up-left" data-aos-duration="1600" class="rocket" alt="">
                </div>
            </div>
            <div class="arrow-animated"></div>
        </div>

    </header>

    <section>
        <div class="container-fluid sezione2 py-5">
            <div class="container py-5">
                <div class="row h-50 align-items-center py-5">
                    <div class="col-12 text-center pt-lg-5 my-lg-5">
                        <h2 class="ff-p tit1-sezione-2" data-aos="fade-down" data-aos-duration="800">{{__('ui.welcome5')}}<span class="ff-p tit2-sezione-2">{{__('ui.welcome6')}}</span></h2>
                    </div>
                </div>
                <div class="row h-50 justify-content-center">
                    @foreach ($articles as $article)
                        
                            <div class="col-12 col-md-4 mt-5 mb-3 d-flex justify-content-center">
                                <div class="card card-container text-center positione-relative cardAnimation" data-aos="fade-right" data-aos-duration="600">
                                    <a href="{{route('category.show', $article->category)}}" class="text-decoration-none text-reset">
                                        <p class="text-white small category-decoration"> {{ $article->category->name }}</p>
                                    </a> 
                                    <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(300, 300) : '../../media/no-image300x300.jpg'}}" alt="" class="img-top card-image">
                                <div class="card-body d-flex flex-column justify-content-between">
                                    <h3 class="article-title text-white">{{ $article->title }}</h3>
                                    <h2 class="price">€{{ $article->price }}</h2>
                                    <a href="{{ route('article.show', compact('article')) }}" class="text-decoration-none ">
                                        <button class=" bottone-card p-2 px-3">{{__('ui.card1')}}</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
                        
                        
                                    
                                        
                                       

 

    <section>
        <div class="container-fluid register-section-bg">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 text-center pt-xl-5">
                        @auth
                            <p class="tit1-sezione-2 ff-p h2 text-white pt-xl-5" data-aos="fade-down" data-aos-duration="800">{{__('ui.welcome7')}}</p>
                            <p class="tit2-sezione-2 h2 ff-p" data-aos="fade-down" data-aos-duration="800" data-aos-delay="200">{{__('ui.welcome8')}}</p>
                            <a href="{{route('article.create')}}"><button class="bottone-banner mt-5" data-aos="fade" data-aos-duration="800" data-aos-delay="400">{{__('ui.welcome9')}}</button></a>
                        @else
                            <p class="tit1-sezione-2 h2 text-white ff-p" data-aos="fade-down" data-aos-duration="800">{{__('ui.welcome10')}}</p>
                            <p class="tit2-sezione-2 h2 ff-p" data-aos="fade-down" data-aos-duration="800" data-aos-delay="200">{{__('ui.welcome11')}}</p>
                            <a href="{{route('register')}}"><button class="bottone-banner mt-5" data-aos="fade" data-aos-duration="800" data-aos-delay="400">{{__('ui.welcome12')}}</button></a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section>
        <div class="container-fluid work-with-us-section">
            <div class="container h-100">
                <div class="row h-100 align-items-center text-center">
                    <div class="col-12">
                        <p class="tx-s h2 display-5 ff-p tx-shadow tit1-sezione-2" data-aos="fade-down" data-aos-duration="800">{{__('ui.welcome13')}}<span class="tx-p display-5 ff-p tx-shadow tit2-sezione-2">{{__('ui.welcome14')}}</span></p>
                        <p class="ff-s my-5 sub-banner-2 text-center" data-aos="fade-down" data-aos-duration="800">{{__('ui.welcome15')}}</p>
                        <a href="{{route('work-with-us')}}"><button class="bottone-banner" data-aos="fade-in" data-aos-delay="200" data-aos-duration="800">{{__('ui.welcome16')}}</button></a>
                    </div>
                </div>
            </div>
        </div>



    </section>





</x-layout>