<x-layout>

<div class="container my-5">
    <div class="row my-5 justify-content-center">
        @forelse ($articles as $article)                 
            <div class="col-12 col-md-3 mt-5 mb-3 d-flex justify-content-center">
                <div class="card card-container text-center positione-relative">
                    <a href="{{route('category.show', $article->category)}}" class="text-decoration-none text-reset">
                        <p class="text-white small category-decoration"> {{ $article->category->name }}</p>
                    </a> 
                    <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(300, 300) : '../../media/no-image300x300.jpg'}}" alt="" class="img-top card-image">
                    <div class="card-body d-flex flex-column justify-content-between">
                        <h3 class="article-title text-white">{{ $article->title }}</h3>
                        <h2 class="price">€{{ $article->price }}</h2>
                        <div>
                            <a href="{{ route('article.show', compact('article')) }}" class="text-decoration-none ">
                                <button class=" bottone-card p-2 px-3">{{__('ui.card1')}}</button>
                            </a>
                            {{-- <p class="mb-0 utente pt-1">Pubblicato da {{ $article->user->name }}</p>
                            <p class="mb-0 pb-0 utente">Il {{ $article->created_at->format('d/m/y') }}</p> --}}
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-12 text-center py-5">
                <h2 class="tx-shadow ff-p tx-s">{{__('ui.article-search')}}</h2>
            </div>           
        @endforelse

        @if(count($articles))
            <div class="container my-5 py-5">
                <div class="row justify-content-center">
                <div class="col-12 d-flex justify-content-center align-items-center">
                    {{$articles->links()}}
                </div>
                </div>
            </div>
        @endif
    </div>
</div>



</x-layout>