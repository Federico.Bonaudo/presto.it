<x-layout>

  <div class="container-fluid mt-5 general-background min-vh-100 d-flex align-items-center">         
    <div class="container my-5">
        <div class="row">
    

            <div class="col-12 col-md-6 p-0 colCarousel">
              
              @if(count($article->images))

              <!-- Carousel wrapper -->
              <div id="carouselMDExample" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <!-- Slides -->
                <div class="carousel-inner mb-5 shadow-1-strong">
                  @foreach($article->images as $image)
                  <div class="carousel-item @if($loop->first) active @endif">
                    <img src="{{$image->getUrl1(600, 600)}}" class="d-block img-col-show" alt="..." />
                  </div>
                  @endforeach
                </div>
                <!-- Slides -->

                <!-- Controls -->
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselMDExample"
                  data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselMDExample"
                  data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
                <!-- Controls -->

                <!-- Thumbnails -->
                <div class="carousel-indicators carousel-indicators-custom">
                  @foreach($article->images as $image)
                  <button type="button" data-bs-target="#carouselMDExample" class="bg-transparent slideCustom @if($loop->first) active @endif"
                    aria-current="true">
                    <img class="d-block w-100 shadow-1-strong rounded"
                      src="{{$image->getUrl1(600, 600)}}" class="img-fluid" />
                  </button>
                  @endforeach
                </div>
                <!-- Thumbnails -->
              </div>            
              @else
                  <div class="carousel-inner">
                      <div class="carousel-item active">
                          <img src="../../media/no-image600x600.jpg" class="d-block w-100 img-col-show" alt="second image">
                      </div>
                  </div>
              @endif
            </div>
            <div class="col-12 col-md-6 p-5 bg-light ff-s colCarousel txt-col-show d-flex flex-column justify-content-between">
                <p class="ms-auto small text-end"> {{__('ui.card')}} {{ $article->category->name }}</p> 
                <h3 class="fw-bold my-3 tx-p mt-5">{{ $article->title }}</h3>
                <h2 class="fw-normal mt-5 display-6 my-5">€{{ $article->price }}</h2>
                <p class="mb-0 fs-5 mt-3">{{__('ui.card5')}}</p>
                <hr class="mt-0">
                <p class="mb-5">{{$article->description}}</p>
                <div class="mt-5">
                  <a href="{{ route('article.index') }}" class="text-decoration-none ">
                    <button class=" bottone-card fs-5 p-2">{{__('ui.card6')}} </button>
                  </a>
                </div>    
            </div>
        </div>    
    </div>
  </div>









</x-layout>