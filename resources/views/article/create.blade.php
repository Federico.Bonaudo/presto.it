<x-layout>
    <div class="container-fluid pt-5">
        <div class="row justify-content-center w-100 pt-5">
            <div class="col-12 col-md-6 mt-3">
                @livewire('article-create-form')
            </div>
        </div>
    </div>
</x-layout>
