<x-layout>

<x-slot name="title">Articles</x-slot>

    <div class="container-fluid min-vh-100">
        <div class="container my-5">
            <div class="row justify-content-center">
                @livewire('article-list')
            </div>
        </div>
    </div>



</x-layout>