<x-layout>
    <div class="container-fluid bg-grey min-vh-100 d-flex align-items-center">
        <x-session-messages />
        <div class="row my-5 justify-content-center w-100">
            <div class="col-12 col-md-6">
                @livewire('become-revisor-form')
            </div>
        </div>
    </div>
</x-layout>