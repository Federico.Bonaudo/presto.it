<x-layout>

    <section>
        <div class="container-fluid min-vh-100 d-flex align-items-center bg-grey">
            <x-session-messages />
            <div class="container blurBox text-white ">
                <div class="row h-100 pt-5">
                    <div class="col-12 col-lg-6 p-5">
                        <div class="d-flex justify-content-end pe-2 ff-s">
                            <a href="{{route('login')}}" class="auth-links">{{__('ui.login')}}</a>
                            <a href="{{route('register')}}" class="auth-links">{{__('ui.login1')}}</a>
                        </div>
                        <div class="pt-5 text-center font-p">
                            <h1 class="tx-shadow display-4 accedi-registrati ff-p">{{__('ui.login2')}}</h1>
                        </div>
                        <x-validation-errors />
                        
                        <form action="{{route('login')}}" method="POST" class="authentication mt-5 d-flex flex-column justify-content-center tx-shadow">
                            @csrf
                            <div class="mb-3">
                                <label for="email" class="form-label">{{__('ui.login3')}}</label>
                                <input type="email" name="email" class="form-control shadow" id="email" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">{{__('ui.login4')}}</label>
                                <input type="password" name="password" class="form-control shadow" id="password">
                            </div>
                            <div class="mb-3 form-check">
                                <input type="checkbox" class="form-check-input" name="remember" id="remember">
                                <label class="form-check-label" for="remember">{{__('ui.login5')}}</label>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="bottone-auth mt-5">{{__('ui.login6')}}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-lg-6 d-none d-md-flex justify-content-center align-items-center h-100 order-sm-last">
                        <div class="boxImg">
                            <div class="imgAuth h-100 w-100"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</x-layout>