<x-layout>
    {{-- <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <form class="p-5 shadow" action="{{route('register')}}" method="POST">
                    <x-validation-errors />
                @csrf
                    <div class="mb-3">
                        <label for="email" class="form-label">Indirizzo email</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <label for="name" class="form-label">Nome</label>
                            <input type="text" name="name" class="form-control" id="name">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="surname" class="form-label">Cognome</label>
                            <input type="text" name="surname" class="form-control" id="surname">
                        </div>
                    </div>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label">Conferma password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                    </div>
                    <button type="submit" class="btn btn-primary">Registrati</button>
                </form>
            </div>
        </div>
    </div> --}}


    <section>
        <div class="container-fluid min-vh-100 d-flex align-items-center bg-grey">
            <div class="container blurBox">
                <div class="row h-100">
                    <div class="col-12 col-lg-6 d-none d-md-flex justify-content-center align-items-center colonna-immagine order-md-last">
                        <div class="boxImg">
                            <div class="imgAuth h-100 w-100"></div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-5 text-white">
                        <div class="d-flex justify-content-end me-5 pe-2 ff-s">
                            <a href="{{route('login')}}" class="auth-links">{{__('ui.register')}}</a>
                            <a href="{{route('register')}}" class="auth-links">{{__('ui.register1')}}</a>
                        </div>
                        <div class="pt-5 text-center">
                            <h1 class="display-4 tx-shadow ff-p accedi-registrati">{{__('ui.register2')}}</h1>
                            <x-validation-errors />
                        </div>
                        <form class="authentication ff-s tx-shadow mt-5 px-md-5 d-flex flex-column justify-content-center order-md-first" action="{{route('register')}}" method="POST">
                        @csrf
                            <div class="mb-3">
                                <label for="email" class="form-label">{{__('ui.register3')}}</label>
                                <input type="email" name="email" class="form-control shadow" id="email">
                            </div>
                            <div class="d-md-flex justify-content-between">
                                <div class="register-name mb-3 w-100 me-md-1">
                                    <label for="name" class="form-label">{{__('ui.register4')}}</label>
                                    <input type="text" name="name" class="form-control shadow" id="name">
                                </div>
                                <div class="register-name mb-3 w-100 ms-md-1">
                                    <label for="surname" class="form-label">{{__('ui.register5')}}</label>
                                    <input type="text" name="surname" class="form-control shadow" id="surname">
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">{{__('ui.register6')}}</label>
                                <input type="password" name="password" class="form-control shadow" id="password">
                            </div>
                            <div class="mb-3">
                                <label for="password_confirmation" class="form-label">{{__('ui.register7')}}</label>
                                <input type="password" name="password_confirmation" class="form-control shadow" id="password_confirmation">
                            </div>
                            <div class="text-center">
                                <button class="bottone-auth mt-5">{{__('ui.register8')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-layout>