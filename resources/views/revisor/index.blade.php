<x-layout>
    <div class="container-fluid min-vh-100 bg-grey">
        <div class="container my-5 pt-5">
            <x-session-messages />
            <div class="row pt-5">
                <div class="col-12">
                    <h1 class="text-center tx-s tx-shadow">
                        {{ $article_to_check ? 'Annunci da revisionare' : 'Non ci sono annunci da revisionare' }}</h1>
                    </div>
                </div>
            </div>
            
            <div class="container my-5">

                @if ($article_to_check)
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-md-6 d-flex flex-column align-items-center carouselRevisor">                            
                        @if (count($article_to_check->images))
                            <div id="carouselExampleIndicators" class="carousel slide carousel-fade indicatorRevisor" data-bs-ride="carousel">
                                <div class="carousel-inner mb-5 shadow-1-strong">
                                    @foreach ($article_to_check->images as $image)
                                        <div class="carousel-item image-revisor @if($loop->first) active @endif">
                                            <img src="{{$image->getUrl1(600, 600)}}" class="d-block" alt="..." />
                                        </div>
                                    @endforeach
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                                <div class="carousel-indicators carousel-indicators-custom">
                                    @foreach($article_to_check->images as $image)
                                        <button type="button" data-bs-target="#carouselExampleIndicators" class="bg-transparent slideCustom @if($loop->first) active @endif"
                                            aria-current="true">
                                            <img class="d-block w-100 shadow-1-strong rounded"
                                            src="{{$image->getUrl1(600, 600)}}" class="img-fluid" />
                                        </button>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="../../media/no-image600x600.jpg" class="d-block w-100"
                                    alt="second image">
                                </div>
                            </div>            
                        @endif
                        <h5 class="card-title my-3 tx-s">{{ __('ui.form') }}: {{ $article_to_check->title }}</h5>
                        <p class="card-text mb-3">{{ __('ui.form5') }}: {{ $article_to_check->description }}</p>
                        <p class="card-footer text-muted small fst-italic mb-3">{{ __('ui.published') }}:
                            {{ $article_to_check->created_at->format('d/m/Y') }}</p>
                    </div>
                
                    <div class="d-flex justify-content-center align-items-center">
                        <form action="{{ route('revisor.accept_article', ['article' => $article_to_check]) }}"
                            method="POST" class="mx-3">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="refuse-button p-1 px-3">{{ __('ui.accept') }}</button>
                        </form>
                        
                        <form action="{{ route('revisor.reject_article', ['article' => $article_to_check]) }}"
                            method="POST" class="mx-3">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="accept-button p-1 px-3">{{ __('ui.deny') }}</button>
                        </form>
                    </div>
                </div>
                @endif
            </div>



@if ($article_to_check)
@if ($article_to_check->images)
<div class="container mb-5 pb-5">
    <div class="row justify-content-center py-5">
        <div class="col-12 d-flex flex-column align-items-start table-wrapper-x">
            <h4 class="mb-5">
                <p class="tx-s tx-shadow mb-0">{{ __('ui.revisor') }} <span
                    class="tx-p tx-shadow">{{ __('ui.revisor1') }}</span></p>
                </h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('ui.revisor2') }}</th>
                            <th scope="col">{{ __('ui.revisor3') }}</th>
                            <th scope="col">{{ __('ui.revisor4') }}</th>
                            <th scope="col">{{ __('ui.revisor5') }}</th>
                            <th scope="col">{{ __('ui.revisor6') }}</th>
                            <th scope="col">{{ __('ui.revisor7') }}</th>
                            <th scope="col">Tags</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($article_to_check->images as $image)
                        @if ($image->labels != null)
                        <tr>
                            <th scope="row">{{ $foto_index++ }}</th>
                            <td><span class="{{ $image->adult }}"></span></td>
                            <td><span class="{{ $image->medical }}"></span></td>
                            <td><span class="{{ $image->spoof }}"></span></td>
                            <td><span class="{{ $image->violence }}"></span></td>
                            <td><span class="{{ $image->racy }}"></span></td>
                            @if ($image->labels)
                            <td>
                                @foreach ($image->labels as $label)
                                {{ $label }},
                                @endforeach
                            </td>
                            @endif
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
    @endif
    
    
    
    <div class="container my-5">
        <div class="row my-5 justify-content-center">
            <h4 class="mb-5">
                @if(Auth::user()->id != 1)
                <p class="tx-s tx-shadow mb-0">{{ __('ui.revisor8') }} <span class="tx-p tx-shadow">
                    {{ __('ui.revisor9') }}</span></p>
                @else
                <p class="tx-s tx-shadow mb-0">{{ __('ui.revisor89') }} <span class="tx-p tx-shadow">
                    {{ __('ui.revisor9') }}</span></p>
                @endif
                </h4>
                <div class="col-12 table-wrapper">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">{{ __('ui.revisor10') }}</th>
                                <th scope="col">{{ __('ui.revisor11') }}</th>
                                <th scope="col">{{ __('ui.revisor12') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                           @if(Auth::user()->id != 1) 
                            @foreach ($article_checked as $article)
                            <tr>
                                <th scope="row">{{ $i-- }}</th>
                                <td>{{ $article->title }}</td>
                                <td>
                                    @if ($article->is_accepted == 1)
                                    <p class="text-success">{{ __('ui.accepted') }}</p>
                                    @else
                                    <p class="text-danger">{{ __('ui.denied') }}</p>
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('revisor.undo_article', compact('article')) }}"
                                    method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit"
                                    class="btn btn-outline-danger">{{ __('ui.annulla') }}</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                       @else
                       @foreach ($article_checked_admin as $article)
                       <tr>
                           <th scope="row">{{ $i-- }}</th>
                           <td>{{ $article->title }}</td>
                           <td>
                               @if ($article->is_accepted == 1)
                               <p class="text-success">{{ __('ui.accepted') }}</p>
                               @else
                               <p class="text-danger">{{ __('ui.denied') }}</p>
                               @endif
                           </td>
                           <td>
                               <form action="{{ route('revisor.undo_article', compact('article')) }}"
                               method="POST">
                               @csrf
                               @method('PATCH')
                               <button type="submit"
                               class="btn btn-outline-danger">{{ __('ui.annulla') }}</button>
                           </form>
                       </td>
                   </tr>
                   @endforeach
                   @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





</x-layout>
