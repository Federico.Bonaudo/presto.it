<!DOCTYPE html>
<html 
    @switch(app()->getLocale())
        @case('en')
            lang='en'
            @break
        @case('es')
            lang='es'
            @break
        @default
            lang="it"
    @endswitch
    >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="x-icon" href="../../media/RocketPNG.png">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title ?? 'Presto.it'}}</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    @livewireStyles
</head>
<body>
    <x-navbar />

    


    <div class="floating-container">
        <div class="floating-button bi bi-globe-europe-africa"></div>
        <div class="element-container">  
            @switch(app()->getLocale())
            @case('en')
                <a href="#"><span class="float-element tooltip-left"><x-_locale lang='it' /></span></a>
                <span class="float-element background-orange"><x-_locale lang='en' /></span>
                <span class="float-element"><x-_locale lang='es' /></span>
            @break
            @case('es')
                <a href="#"><span class="float-element tooltip-left"><x-_locale lang='it' /></span></a>
                <span class="float-element"><x-_locale lang='en' /></span>
                <span class="float-element background-orange"><x-_locale lang='es' /></span>
            @default
                <a href="#"><span class="float-element tooltip-left background-orange"><x-_locale lang='it' /></span></a>
                <span class="float-element"><x-_locale lang='en' /></span>
                <span class="float-element"><x-_locale lang='es' /></span>
             @endswitch    
           
        </div>
    </div>

    <div class="min-vh-100">
        {{$slot}}
    </div>


    <x-footer />

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
    @livewireScripts
</body>
</html>