@if(session('errorMessage'))
    <div class="rectangle" id="rectangle">
        <div class="notification-text pe-1">
            <i class="bi bi-exclamation-triangle-fill fs-5 px-2"></i>
            <span>{{session('errorMessage')}}</span>
        </div>
    </div>
@endif

@if(session('successMessage'))
    <div class="rectangle" id="rectangle">
        <div class="notification-text pe-1">
            <i class="bi bi-info-circle-fill fs-5 px-2"></i>
            <span>{{session('successMessage')}}</span>
        </div>
    </div>
@endif