<form action="{{route('set_language_locale', $lang)}}" method="POST" class="d-inline">
@csrf
<button type="submit" class="btn p-0">
    <img src="{{asset('vendor/blade-flags/language-'. $lang . '.svg')}}" width="24" height="24">
</button>
</form>