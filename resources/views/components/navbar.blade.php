<nav id="nav" class="navbar fixed-top navbar-expand-lg bg-body-tertiary navbar-dark bgNavbar">
  <div class="container-fluid">
    <a class="navbar-brand ms-md-5" href="{{route('homepage')}}"><img class="navLogo" src="../../media/logo-navbar-transformed.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navAnimation navbar-nav mx-auto mb-2 ps-lg-5 mb-lg-0 align-items-lg-center">
            <li class="nav-item">
                <a class="nav-link px-4 text-white @if(Route::is('homepage')) active_nav @endif d-inline-block" aria-current="page" href="{{route('homepage')}}">Home</a>
            </li>
            <li class="nav-item">

                <a class="nav-link px-4 text-white @if(Route::is('article.index')) active_nav @endif d-inline-block" href="{{route('article.index')}}">{{__('ui.nav')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link px-4 text-white @if(Route::is('article.create')) active_nav @endif d-inline-block" href="{{route('article.create')}}">{{__('ui.nav1')}}</a>
            </li>
            
            <li class="nav-item dropdown px-4">
              <a class="nav-link dropdown-toggle text-white" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                {{__('ui.nav2')}}
              </a>
              <ul id="dropCategory" class="navAnimation dropdown-menu dropDownBcg dropDownScroll">
                @foreach ($categories as $category)
                  <li class=""><a class="dropdown-item text-white" href="{{route('category.show' , compact('category'))}}">{{__('ui.category'.$index++)}}</a></li>
                  <li><hr class="dropdown-divider"></li>
                @endforeach                
              </ul>
            </li>
        </ul>
        <ul class="navbar-nav ms-auto ms-xl-5 me-lg-5 mb-2 mb-lg-0 align-items-lg-center">
          <li class="box nav-item">
            <div class="search-container me-lg-4 d-none d-md-none d-lg-block">
              <form action="{{route('article.search')}}" method="GET">
                <input class="search expandright" id="searchright" type="text" aria-label="Search" name="searched" placeholder="Cerca">
                <label class="btnSearch searchbutton" for="searchright"><span class="mglass">&#9906;</span></label>
              </form>
            </div>
            <div class="search-container d-md-block d-lg-none">
              <form action="{{route('article.search')}}" method="GET">
                <input class="search" id="searchleft" type="text" aria-label="Search" name="searched" placeholder="Cerca">
                <label class="btnSearch searchbutton" for="searchleft"><span class="mglass">&#9906;</span></label>
              </form>
            </div>
          </li>
          @auth
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white px-4" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{__('ui.nav3') . Auth::user()->name}}
              @if(Auth::user() && Auth::user()->is_revisor)
                @if (App\Models\Article::toBeRevisionedCount() == 0)
                  <span class="position-absolute top-0 start-md-100 translate-middle badge rounded-pill bg-s d-none">
                    {{App\Models\Article::toBeRevisionedCount()}}
                    <span class="visually-hidden">unread messages</span>
                  </span>
                @else
                  <span class="position-absolute top-0 start-md-100 translate-middle badge rounded-pill bg-s">
                    {{App\Models\Article::toBeRevisionedCount()}}
                    <span class="visually-hidden">unread messages</span>
                  </span>                  
                @endif
              @endif
            </a>
            <ul id="dropUser" class="navAnimation overflow-hidden dropdown-menu dropDownBcg">
              <li class=""><a class="dropdown-item text-white" href="{{route('profile')}}">{{__('ui.nav4')}}</a></li>
              @if (Auth::user() && Auth::user()->is_revisor)
                <li>
                  <a href="{{route('revisor.index')}}" class="position-relative dropdown-item text-white" aria-current="page">
                    {{__('ui.nav5')}}
                    @if (App\Models\Article::toBeRevisionedCount() == 0)
                    <span class="position-absolute top-0 start-md-100 translate-middle badge rounded-pill bg-s d-none">
                      {{App\Models\Article::toBeRevisionedCount()}}
                      <span class="visually-hidden">unread messages</span>
                      </span>
                    @else
                      <span class="position-absolute top-0 start-md-100 translate-middle badge rounded-pill bg-s">
                        {{App\Models\Article::toBeRevisionedCount()}}
                        <span class="visually-hidden">unread messages</span>
                      </span>                  
                    @endif
                  </a>
                </li>
              @endif
                <li><hr class="dropdown-divider"></li>
              <li><a class="dropdown-item text-white" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">Logout</a></li>
              <form action="{{route('logout')}}" id="form-logout" method="POST" class="d-none">@csrf</form>
            </ul>
            @else   
            <li><a href="{{route('login')}}" class="btnAccedi px-4">{{__('ui.nav6')}}</a></li>
          @endauth

        </ul>
    </div>
  </div>
</nav>

