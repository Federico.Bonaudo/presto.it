<footer id="foot" class="text-center text-lg-start text-muted">
  <!-- Section: Social media -->
  <section class="d-flex justify-content-center p-4 border-bottom">
    <div class="icon-effect icon-effect-1 d-flex justify-content-center">
      <a href="#" class="me-4 text-decoration-none icon text-center fs-5">
        <i class="bi bi-facebook icon-footer"></i>
      </a>
      <a href="#" class="me-4 text-decoration-none icon text-center fs-5">
        <i class="bi bi-twitter icon-footer"></i>
      </a>
      <a href="#" class="me-4 text-decoration-none icon text-center fs-5">
        <i class="bi bi-instagram icon-footer"></i> </a>
        <a href="#" class="text-decoration-none icon fs-5">
          <i class="bi bi-linkedin icon-footer"></i> </a>
        </div>
        <!-- Right -->
      </section>
      <!-- Section: Social media -->
      
      <!-- Section: Links  -->
      <section class="">
        <div class="container text-center text-md-start mt-5">
          <!-- Grid row -->
          <div class="row mt-3">
            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
              <!-- Content -->
              <h6 class="text-uppercase fw-bold mb-4">
              </h6>
              <div class="logo justify-content-start align-items-center">
                <a href="{{route('homepage')}}">
                  <img src="../../media/logo-footer-transformed.png" alt="" class="img-fluid">
                </a>
              </div>
            </div>
            <!-- Grid column -->
            
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4 align-items-start">
              <!-- Links -->
              <h6 class="text-uppercase fw-bold mb-4 title">
                {{__('ui.footer')}} 
              </h6>
              @for ($category = 1; $category <= 4; $category++)
              <p><a href="{{route('category.show' , compact('category'))}}" class="text-decoration-none link footLink ">{{__('ui.category'.  $category)}}</a></p>
              @endfor
            </div>
            <!-- Grid column -->
            
            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4 align-items-start">
              <!-- -->
              <h6 class="text-uppercase fw-bold mb-4 title">
                {{__('ui.footer5')}} 
              </h6>
              <p>
                <a href="{{route('article.create')}}" class="footLink text-decoration-none link">{{__('ui.footer6')}}</a>
              </p>
              <p>
                <a href="{{route('article.index')}}" class="footLink text-decoration-none link">{{__('ui.footer7')}}</a>
              </p>
              <p>
                <a href="{{route('work-with-us')}}" class="footLink text-decoration-none link">{{__('ui.footer8')}}</a>
              </p>
              
              @if(Auth::user())
                <p>
                  <a href="{{route('profile')}}" class="footLink text-decoration-none link">{{__('ui.footer9')}}</a>
                </p>
              
              @else
                <p>
                  <a href="{{route('login')}}" class="footLink text-decoration-none link">{{__('ui.footer10')}}</a>
                </p>
              
              
              @endif
              
            </div>
            <!-- Grid column -->
            
            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4 align-items-start">
              <!-- Links --> <div class="link">
                <h6 class="text-uppercase fw-bold mb-4 title">{{__('ui.footer11')}}</h6>
                <p class=" text-decoration-none 
                "> Bari, BA 70125, IT</p>
                <p class=" 
                ">
                  info@presto.it
                </p>
                <p class=" 
                "> +39 234 567 8867</p>
                <p class=" 
                "> 080 234 5678</p>
              </div>
            </div>
            <!-- Grid column -->
          </div>
          <!-- Grid row -->
        </div>
      </section>
      <!-- Section: Links  -->
      
      <!-- Copyright -->
      <div class="text-center p-4 copyright">
        © 2023 Copyright: NeverCodeAlone
      </div>
      <!-- Copyright -->
    </footer>