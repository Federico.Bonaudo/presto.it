{{-- <!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Richiesta revisore</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cuprum&family=Nobile&display=swap" rel="stylesheet">
</head>
<style>
   body {
    font-family: 'Cuprum';
background-color: #FF9A32;
    position: relative;
}

/* header */
.onde {
    background-image: url('waveviola.svg');
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: 30vh;
    background-repeat: repeat-x;
}
/* testo */
.text {
    display: flex;
    justify-content: center;
    color: white;
    text-shadow: #582e98a9;
    margin-bottom: 10vh;
}
.title-mail{
    font-family: 'Cuprum';
     color: white;
    font-size: 4.2vh;
    text-shadow: 1px 1px 1px rgba(40, 40, 40, 0.853);}
.revisor{
    font-family: 'Cuprum';
    color:rgb(80, 27, 159);   
    font-size: 4.2vh;
   text-shadow: 1px 1px 1px rgba(40, 40, 40, 0.815);
}

h2 {
    font-size: 3vh;
    font-family: 'Cuprum';
    color: #592E98;
}
.dati{
    font-size: 3vh;
    justify-content: start;
    font-family: 'Cuprum';
    color: #FFFFFF;
    text-shadow: 1px 0.8px 0.8px rgb(37, 34, 34);
}

/* footer */
footer {
    background-color: #592E97;
    font-family: 'Nobile', sans-serif;
    width: 100vw;
}

.copyright {
    background-color: rgba(0, 0, 0, 0.05);
    color: white;
}

.logo {
    width: 40%;
    transition: 0.2s;
}
.logo:hover{
    scale: 1.02;
}

/* bottone */
.bottone-banner {
    background: rgb(89, 46, 152);
    box-shadow: rgba(0, 0, 0, 0.17) 0px -10px 15px 0px inset, rgba(0, 0, 0, 0.15) 0px -36px 30px 0px inset, rgba(0, 0, 0, 0.1) 0px -79px 40px 0px inset, rgba(0, 0, 0, 0.06) 0px 2px 1px, rgba(0, 0, 0, 0.09) 0px 4px 2px, rgba(0, 0, 0, 0.09) 0px 8px 4px, rgba(0, 0, 0, 0.09) 0px 16px 8px, rgba(0, 0, 0, 0.09) 0px 32px 16px;
    border-radius: 50px;
    padding: 10px 30px;
    font-family: 'Cuprum';
    text-align: center;
    font-size: 2vh;
    color: #FFFFFF;
    border: none;
    transition: 0.2s;
    text-decoration: none;
}

.bottone-banner:focus,
.bottone-banner:active {
    box-shadow: rgb(36, 13, 71) 3px 3px 6px 0px inset, rgba(41, 16, 78, 0.5) -3px -3px 6px 1px inset;
}

.bottone-banner:hover {
    background: rgb(80, 27, 159);
}
</style>

<body>
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 mb-5">
                <div class="onde">

                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 mt-5">
                <div class="text">
                    <div class="body justify-content-center">
                        <div class="title-mail text-center justify-content-center">
                            <h1>Un utente ha richiesto di diventare Revisor</h1>
                        </div>
                        <div class="description text-center">
                            <h2>Ecco i suoi dati:</h2>
                            <div class="dati d-flex-block justify-content-start">
                                <p>Nome: {{ $user->name }}</p>
                                <p>Cognome: {{ $user_data['surname'] }}</p>
                                <p>Email: {{ $user_data['email'] }}</p>
                                <p>Numero di telefono: {{ $user_data['phone_number'] }}</p>

                                <p>Se vuoi renderlo revisore clicca qui:</p>
                            </div>
                            <a href="{{ route('make.revisor', compact('user')) }}" class="text-decoration-none"><button
                                    class="bottone-banner mt-3"> Rendi
                                    revisore</button> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <footer>
        <div class="container text-center">
            <!-- Grid row -->
            <div class="row mt-3 text-center justify-content-center align-items-center">
                <!-- Grid column -->
                <div class=" col-12 col-md-6 text-center mt-3">
                    <div class="logo justify-content-center mx-auto align-items-center">
                        <a href="{{ route('homepage') }}">
                            <img src="../../public/media/logo-footer.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

            </div>
            <!-- Grid column -->
        </div>
        </div>

        <div class="text-center p-4 copyright">
            © 2023 Copyright: NeverCodeAlone
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
</body>

----------- --}}

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cuprum:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Carrois+Gothic&display=swap" rel="stylesheet">
  </head>
  <body>
    <style>
        body {
            margin: 0;
            padding: 0;
        }


        * {
            font-family: 'Carrois Gothic', sans-serif;
        }

        :root {
            --p-color: #592E98;
            --s-color: #fa820d;
        }

        .ff-p {
            font-family: 'Cuprum', sans-serif;
        }

        .tx-p {
            color: var(--p-color);
        }

        .tx-s {
            color: var(--s-color);
        }

        .tx-shadow {
            text-shadow: 1px 1px 1px rgb(37, 34, 34);
        }

        .bottone-banner {
            background: rgb(89, 46, 152);
            box-shadow: rgba(0, 0, 0, 0.17) 0px -10px 15px 0px inset, rgba(0, 0, 0, 0.15) 0px -36px 30px 0px inset, rgba(0, 0, 0, 0.1) 0px -79px 40px 0px inset, rgba(0, 0, 0, 0.06) 0px 2px 1px, rgba(0, 0, 0, 0.09) 0px 4px 2px, rgba(0, 0, 0, 0.09) 0px 8px 4px, rgba(0, 0, 0, 0.09) 0px 16px 8px, rgba(0, 0, 0, 0.09) 0px 32px 16px;
            border-radius: 50px;
            padding: 10px 30px;
            text-align: center;
            color: #FFFFFF;
            border: none;
            transition: 0.2s;
            font-size: 1.5em;
            margin: 20px 0px;
        }

        .bottone-banner:focus,
        .bottone-banner:active {
            box-shadow: rgb(36, 13, 71) 3px 3px 6px 0px inset, rgba(41, 16, 78, 0.5) -3px -3px 6px 1px inset;
        }

        .bottone-banner:hover {
            background: rgb(80, 27, 159);
        }

        .footer {
            position: absolute;
            bottom: 0;
            left: 0;
            text-align: center;
            padding: 10px;
            width: 100%;
            background-color: #592E98;
            color: white;
        }

        .colonna-mail{
            height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            text-align: center;
            padding: 10px;
        }

        .immagine-mail{
            position: absolute;
            top: 10px;
            height: 50px;
            left: 50%;
            transform: translateX(-50%);
        }
    </style>


    <div class="container-fluid bg-mail position-relative vh-100">

        <img src="{{$message->embed(public_path() . '/media/logo-navbar-transformed.png')}}" alt="" class="immagine-mail">

        <div class="row h-100">
            <div class="col-12 text-center colonna-mail">
                <h1 class="tx-s tx-shadow ff-p display-3">Un utente ha richiesto di diventare <span
                        class="tx-p tx-shadow">Revisor</span></h1>
                <p class="tx-p mt-5 fs-3">Nome:<span class="ms-2 tx-s">{{ $user->name }}</span></p>
                <p class="tx-p fs-3">Cognome:<span class="ms-2 tx-s">{{ $user_data['surname'] }}</span></p>
                <p class="tx-p fs-3">Email:<span class="ms-2 tx-s">{{ $user_data['email'] }}</span></p>
                <p class="tx-p fs-3">Numero di telefono:<span class="ms-2 tx-s">{{$user_data['phone_number']}}</span></p>
                <a href="{{ route('make.revisor', compact('user')) }}" class="text-decoration-none mt-2 fs-4"><button
                        class="bottone-banner mt-3"> Rendi
                        revisore</button> </a>
            </div>
        </div>
        <div class="text-center p-4 footer">
            © 2023 Copyright: NeverCodeAlone
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>

