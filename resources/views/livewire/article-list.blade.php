<div class="container my-5">
  <div class="row justify-content-center">

      @forelse ($articles as $article)                 
        <div class="col-12 col-md-3 mt-5 mb-3 d-flex justify-content-center">
            <div class="card card-container text-center positione-relative">
                <a href="{{route('category.show', $article->category)}}" class="text-decoration-none text-reset">
                    <p class="text-white small category-decoration"> {{ $article->category->name }}</p>
                </a>  
                <img src="{{!$article->images()->get()->isEmpty() ? $article->images()->first()->getUrl(300, 300) : '../../media/no-image300x300.jpg'}}" alt="" class="img-top card-image">
                <div class="card-body d-flex flex-column justify-content-between">
                    <h3 class="article-title text-white">{{ $article->title }}</h3>
                    <h2 class="price">€{{ $article->price }}</h2>
                    <div>
                        <a href="{{ route('article.show', compact('article')) }}" class="text-decoration-none ">
                            <button class=" bottone-card p-2 px-3">{{__('ui.card1')}}</button>
                        </a>
                        {{-- <p class="mb-0 utente pt-1">{{__('ui.card2')}} {{ $article->user->name }}</p>
                        <p class="mb-0 pb-0 utente">{{__('ui.card3')}} {{ $article->created_at->format('d/m/y') }}</p> --}}
                    </div>
                </div>
            </div>
        </div>
      @empty
          <div class="col-12 text-center mt-2">
              <h2 class="tx-shadow ff-p tx-s">{{__('ui.article-list')}}</h2>
          </div>
      @endforelse
      <div class="container mt-5 pt-5">
          <div class="row justify-content-center sticky-bottom">
              <div class="col-12 d-flex justify-content-center align-items-center">
                  {{ $articles->links() }}
              </div>
          </div>
      </div>
  </div>
</div>

