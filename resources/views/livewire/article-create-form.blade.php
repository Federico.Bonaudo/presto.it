<div>  
    <form wire:submit.prevent="store" class="blurForm authentication p-5 m-0 mb-5 text-white tx-shadow" enctype="multipart/form-data">
        
        @csrf
        
        
        
        <div class="mb-3">
            <label for="title" class="form-label">{{__('ui.form')}}</label>
            <input type="text" wire:model.lazy="title" class="shadow form-control @error('title') is-invalid @enderror" id="title">
            @error('title') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="mb-3">
            <label for="category" class="form-label">{{__('ui.form1')}}</label>
            <select wire:model.defer="category" id="category" class="shadow form-control">
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">{{__('ui.form2')}}</label>
            <input type="text" wire:model.lazy="price" class="shadow form-control @error('price') is-invalid @enderror" id="price">
            @error('price') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="mb-3">
            <label for="temporary_images" class="form-label">{{__('ui.form3')}}</label>
            <input wire:model="temporary_images" type="file" name="images" multiple class="form-control shadow @error('temporary_images.*') is-invalid @enderror"
            placeholder="Img"/>
            @error('temporary_images.*')
                <p class="text-danger mt-2">{{$message}}</p>
            @enderror
        </div>
        <div class="mb-3">
            @if(!empty($images))
                <div class="row">
                    <div class="col-12">
                        <label class="form-label">{{__('ui.form4')}}</label>
                        <div class="d-flex border custom-height border-4 border-warning rounded shadow py-4">
                            @foreach ($images as $key => $image)
                                <div class="col my-3 mx-3">
                                    <div class="img-preview mx-auto shadow rounded" style="background-image: url({{$image->temporaryUrl()}});"></div>
                                    <button type="button" class="btn btn-danger shadow d-block text-center mt-2 mx-auto" wire:click="removeImage({{$key}})">X</button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="mb-5">
            <label for="description" class="form-label">{{__('ui.form5')}}</label>
            <textarea wire:model.lazy="description" id="description"  rows="5" class="shadow form-control @error('description') is-invalid @enderror"></textarea>
            @error('description') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="text-center">
            <button type="submit" class="bottone-auth mb-3">{{__('ui.form6')}}</button>
        </div>

    </form>
    <x-session-messages />  

</div>


