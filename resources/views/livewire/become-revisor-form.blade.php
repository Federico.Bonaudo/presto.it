<div>
    <form class="p-5 blurForm authentication" action="{{route('become.revisor')}}" method="POST">
        <x-validation-errors />
        @csrf
        <div class="mb-3">
            <label for="email" class="form-label text-white tx-shadow">{{__('ui.become-revisor')}}</label>
            <input type="email" name="email"  class="shadow form-control" id="email" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <div class="row">
                <div class="col-12 col-md-6">
                    <label for="name" class="form-label text-white tx-shadow">{{__('ui.become-revisor1')}}</label>
                    <input type="text" name="name"  class="shadow form-control" id="name">
                </div>
                <div class="col-12 col-md-6">
                    <label for="surname" class="form-label text-white tx-shadow">{{__('ui.become-revisor2')}}</label>
                    <input type="text" name="surname" class="shadow form-control" id="surname">
                </div>
            </div>
        </div>
            <div class="mb-5">
                <label for="phone_number" class="form-label text-white tx-shadow">{{__('ui.become-revisor3')}}</label>
                <input type="text" class="shadow form-control" name="phone_number" id="phone_number">
            </div>
            <div class="text-center">
                <button type="submit" class="bottone-auth">{{__('ui.become-revisor4')}}</button>
            </div>
            <p class="small fst-italic mt-2 pt-3 mb-0 text-white">{{__('ui.become-revisor5')}}</p>
        </form>

    </div>

    