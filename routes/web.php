<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/article/search', [PublicController::class, 'searchArticles'])->name('article.search');
Route::get('/category/show/{category}' , [PublicController::class, 'show'])->name('category.show');
Route::get('/profile' , [PublicController::class, 'profile'])->name('profile');
Route::post('/language/{lang}',[PublicController::class, 'setLanguage'])->name('set_language_locale');

Route::get('/article/index', [ArticleController::class, 'index'])->name('article.index');
Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
Route::get('/article/show/{article}' , [ArticleController::class , 'show'])->name('article.show');


Route::get('/revisor/home', [RevisorController::class, 'index'])->middleware('isRevisor')->name('revisor.index');
Route::patch('/article/accept/{article}', [RevisorController::class, 'acceptArticle'])->name('revisor.accept_article');
Route::patch('/article/reject/{article}', [RevisorController::class, 'rejectArticle'])->name('revisor.reject_article');
Route::patch('/article/undo/{article}', [RevisorController::class, 'undoArticle'])->name('revisor.undo_article');
Route::get('/work-with-us', [RevisorController::class, 'workWithUs'])->name('work-with-us');
Route::post('/become/revisor', [RevisorController::class, 'becomeRevisor'])->middleware('auth')->name('become.revisor');
Route::get('/make/revisor/{user}', [RevisorController::class, 'makeRevisor'])->name('make.revisor');

