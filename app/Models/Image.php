<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ['path'];
    protected $casts = [
        'labels' => 'array'
    ];

    public function article(){
        return $this->belongsTo(Article::class);
    }
// primo crop
    public static function getUrlByFilePath($filePath, $w = null, $h = null){
        if(!$w && !$h){
            return Storage::url($filePath);
        }
        $path = dirname($filePath);
        $filename = basename($filePath);
        $file = "{$path}/crop_{$w}x{$h}_{$filename}";
        return Storage::url($file);
    }

    public function getUrl($w = null, $h = null){
        return Image::getUrlByFilePath($this->path, $w, $h);
    }
    // secondo crop
    public static function getUrlByFilePath1($filePath, $w1 = null, $h1 = null){
        if(!$w1 && !$h1){
            return Storage::url($filePath);
        }
        $path = dirname($filePath);
        $filename = basename($filePath);
        $file1 = "{$path}/crop_{$w1}x{$h1}_{$filename}";
        return Storage::url($file1);
    }

    public function getUrl1($w1 = null, $h1 = null){
        return Image::getUrlByFilePath($this->path, $w1, $h1);
    }

}
