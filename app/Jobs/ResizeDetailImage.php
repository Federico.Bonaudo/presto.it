<?php

namespace App\Jobs;

use Spatie\Image\Image;
use Illuminate\Bus\Queueable;
use Spatie\Image\Manipulations;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ResizeDetailImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $w1, $h1, $fileName, $path;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filePath, $w1, $h1)
    {
        $this->path = dirname($filePath);
        $this->fileName = basename($filePath);
        $this->w1 = $w1;
        $this->h1 = $h1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $w1 = $this->w1;
        $h1 = $this->h1;
        $srcPath = storage_path() . '/app/public/' . $this->path . '/' . $this->fileName;
        $destPath = storage_path() . '/app/public/' . $this->path . "/crop_{$w1}x{$h1}_" . $this->fileName;
        $croppedImage = Image::load($srcPath)
            ->crop(Manipulations::CROP_CENTER, $w1, $h1)
            ->watermark(base_path('resources/img/watermark.png'))
            ->watermarkPosition(Manipulations::POSITION_CENTER)
            ->watermarkFit(Manipulations::FIT_CONTAIN)
            ->save($destPath);
    }
}
