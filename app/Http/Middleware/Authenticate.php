<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        
        if (! $request->expectsJson()) {

            $localeLanguage = session('locale' , 'it');
            App::setLocale($localeLanguage);
            
            $message = "";
        

            if(app()->getLocale() == 'en'){
                $message = "You must be logged to continue.";
            } else if(app()->getLocale() == 'es'){
                $message = "Debe ser miembro para realizar esta acción. Conéctese ahora";
            } else {
                $message = "Devi essere iscritto per poter compiere questa azione. Accedi ora!";
            }

            session()->flash('errorMessage', $message);
            return route('login');
            
        }
    }
}
