<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsRevisor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check() && Auth::user()->is_revisor){
            return $next($request);
        }

        $message = "";
        
            if(app()->getLocale() == 'en'){
                $message = "Warning! You must be a revisor to access this page!";
            }   else if(app()->getLocale() == 'es'){
                $message = "¡Atención! Debe ser revisor para acceder a esta página.";
            } else {
                $message = "Attenzione! Devi essere un revisore per accedere a questa pagina!";
            }


        return redirect('/')->with('errorMessage', $message);
    }
}
