<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BecomeRevisorForm extends Component
{
    public $name, $surname, $email, $phone_number;

    public function render()
    {
        return view('livewire.become-revisor-form');
    }
}
