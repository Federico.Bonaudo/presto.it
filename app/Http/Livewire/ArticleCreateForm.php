<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\RemoveFaces;
use App\Jobs\ResizeImage;
use App\Jobs\WaterMarkImage;
use Livewire\WithFileUploads;
use App\Jobs\ResizeDetailImage;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ArticleCreateForm extends Component
{
    use WithFileUploads;
    
    public $title, $price, $description, $category = '1', $temporary_images, $images = [], $article;

    protected $rules = [
        'title' => 'required|min:5',
        'price' => 'required',
        'description' => 'required|min:10',
        'images.*' => 'image|max:1024',
        'temporary_images.*' => 'image|max:1024',
    ];

    protected function messages() {
        switch(app()->getLocale()){
            case('en'):
                return $lang = [
                    'title.required' => 'Mandatory field.',
                    'title.min' => 'The title must be at least 5 characters long.',
                    'price.required' => 'Mandatory field.',
                    'description.required' => 'Mandatory field.',
                    'description.min' => 'The description must be at least 10 characters long.',
                    'images.image' => 'The file must be an image',
                    'images.max' => 'The image must be smaller than 1mb',
                    'temporary_images.*.image' => 'The file must be an image',
                    'temporary_images.*.max' => 'The image must be smaller than 1mb',
                ];
            break;
            case('es'):
                return $lang = [
                    'title.required' => 'Campo obligatorio.',
                    'title.min' => 'El título debe tener al menos 5 caracteres.',
                    'price.required' => 'Campo obligatorio.',
                    'description.required' => 'Campo obligatorio.',
                    'description.min' => 'La descripción debe tener al menos 10 caracteres.',
                    'images.image' => 'El archivo debe ser una imagen',
                    'images.max' => 'La imagen debe ocupar como máximo 1mb',
                    'temporary_images.*.image' => 'Los archivos deben ser imágenes',
                    'temporary_images.*.max' => 'La imagen debe ocupar como máximo 1mb',
                ];
            break;
            default:
               return $lang = [
                    'title.required' => 'Campo obbligatorio.',
                    'title.min' => 'Il titolo deve avere almeno 5 caratteri.',
                    'price.required' => 'Campo obbligatorio.',
                    'description.required' => 'Campo obbligatorio.',
                    'description.min' => 'La descrizione deve avere almeno 10 caratteri.',
                    'images.image' => 'Il file deve essere un\'immagine',
                    'images.max' => 'L\'immagine deve essere massimo di 1mb',
                    'temporary_images.*.image' => 'I file devono essere immagini',
                    'temporary_images.*.max' => 'L\'immagine deve essere massimo di 1mb',
                ];
        }
    }

    public function updatedTemporaryimages(){
        if($this->validate([
            'temporary_images.*' => 'image|max:1024',
        ])){
            foreach($this->temporary_images as $image){
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key){
        if(in_array($key, array_keys($this->images))){
            unset($this->images[$key]);
        }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store(){
        
        $this->validate();
        
        $this->article = Category::find($this->category)->articles()->create($this->validate());
        $this->article->user()->associate(Auth::user());
        $this->article->save();
        if(count($this->images)){
            foreach($this->images as $image){

                $newFileName = "articles/{$this->article->id}";
                $newImage = $this->article->images()->create(['path'=>$image->store($newFileName, 'public')]);

                RemoveFaces::withChain([
                    new ResizeImage($newImage->path, 300, 300),
                    new ResizeDetailImage($newImage->path, 600, 600),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id)
                ])->dispatch($newImage->id);

            }
            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }

        $message = "";
    
        if(app()->getLocale() == 'en'){
            $message = "A revisor will publish your article as soon as possible.";
        }   else if(app()->getLocale() == 'es'){
            $message = "Su anuncio está pendiente de revisión. Se publicará lo antes posible.";
        } else {
            $message = 'Il tuo annuncio è in attesa di essere revisionato. Verrà pubblicato al più presto.';
        }


        session()->flash('successMessage', $message);
        $this->reset();
    }

    



    public function render()
    {
        
        return view('livewire.article-create-form');
    }
}


