<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Livewire\Component;

class ArticleList extends Component
{
    public function render()
    {   
        
        $articles = Article::orderBy('created_at' , 'DESC')->where('is_accepted', true)->paginate(8);
        
        return view('livewire.article-list', compact('articles'));
        
    }
}