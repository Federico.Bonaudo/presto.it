<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Mail\BecomeRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function __construct(){
        $this->middleware('auth');        
       }
   
    public function index(){
        $article_to_check = Article::where('is_accepted', null)->orderBy('created_at', 'DESC')->first();
        $article_checked = Article::where('is_accepted', '<>', null)->orderBy('updated_at', 'DESC')->take(20)->get();
        $article_checked_admin = Article::where('is_accepted', '<>', null)->orderBy('updated_at', 'DESC')->get();

        $foto_index = 1;

        $i = 0;
        foreach($article_checked as $article) {
            $i++;
        }
        return view('revisor.index', compact('article_to_check', 'article_checked', 'i', 'foto_index', 'article_checked_admin'));
    }

    public function acceptArticle(Article $article){
        $article->setAccepted(true);

        $message = "";
    
        if(app()->getLocale() == 'en'){
            $message = "You have successfully accepted this article!";
        }   else if(app()->getLocale() == 'es'){
            $message = "Enhorabuena. Has aceptado el artículo!";
        } else {
            $message = 'Complimenti. Hai accettato l\'articolo!';
        }


        return redirect()->back()->with('successMessage', $message);
    }

    public function rejectArticle(Article $article){
        $article->setAccepted(false);

        $message = "";
    
        if(app()->getLocale() == 'en'){
            $message = "You have successfully rejected this article!";
        }   else if(app()->getLocale() == 'es'){
            $message = "Enhorabuena. Has rechazado el artículo!";
        } else {
            $message = 'Ottimo lavoro. Hai rifiutato l\'articolo!';
        }

        return redirect()->back()->with('successMessage', $message);
    }

    public function undoArticle(Article $article){

        if ($article->setAccepted(true) || $article->setAccepted(false)) {
            $article->setAccepted(null);
        }


        $message = "";
    
        if(app()->getLocale() == 'en'){
            $message = "Congratulations. You cancelled the last operation";
        }   else if(app()->getLocale() == 'es'){
            $message = "Enhorabuena. Ha cancelado la última operación";
        } else {
            $message = 'Complimenti. Hai annullato l\'ultima operazione';
        }

        return redirect()->back()->with('successMessage', $message);
    }

    public function becomeRevisor(Request $request){
        $name = $request->name;
        $surname = $request->surname;
        $email = $request->email;
        $phone_number = $request->phone_number;
        $user_data = compact('name', 'surname', 'phone_number', 'email');
        
        $validated = $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
        ]);


        Mail::to('admin@admin.it')->send(new BecomeRevisor(Auth::user(), $user_data));

        $message = "";
    
        if(app()->getLocale() == 'en'){
            $message = "Your request has been sent. We'll write to you back as soon as possible!";
        }   else if(app()->getLocale() == 'es'){
            $message = "Su solicitud ha sido aceptada. Nos pondremos en contacto con usted lo antes posible.";
        } else {
            $message = "La tua richiesta è stata accolta. Ti risponderemo al più presto!";
        }


        return redirect('/')->with('successMessage', $message);
    }

    public function makeRevisor(User $user){
        Artisan::call('presto:makeUserRevisor', ["email" => $user->email]);

        $message = "";
    
        if(app()->getLocale() == 'en'){
            $message = "Congratulations. The user has become a revisor!";
        }   else if(app()->getLocale() == 'es'){
            $message = "Enhorabuena. El usuario se ha convertido en revisor!";
        } else {
            $message = "Complimenti. L'utente è diventato un revisore!";
        }


        return redirect('/')->with('successMessage', $message);
    }
    
    public function workWithUs(){
        return view('work-with-us');
    }
}
