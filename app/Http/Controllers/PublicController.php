<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->except('homepage', 'searchArticles', 'show', 'setLanguage');
    }

    public function homepage(){
        $articles = Article::orderBy('created_at', 'DESC')->where('is_accepted', true)->take(3)->get();
        
        return view('welcome', compact('articles'));
    }

    public function show(Category $category){

        $articles = Article::where('category_id', $category->id)->orderBy('created_at', 'DESC')->paginate(5);

        return view('category.show', compact('category', 'articles'));
    }
    public function searchArticles(Request $request){
        $articles = Article::search($request->searched)->where('is_accepted', true)->paginate(5);

        return view('article.search', compact('articles'));
    }
    public function profile(){

        $user_articles = Article::orderBy('updated_at', 'DESC')->get();
        return view('profile', compact('user_articles'));
    }

    public function setLanguage($lang){
        session()->put('locale', $lang);
        return redirect()->back();
    }
}

